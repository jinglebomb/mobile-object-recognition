/*
 * Copyright(c) 2003-2005 Peter Kovesi
 * School of Computer Science & Software Engineering
 * The University of Western Australia
 * http://www.csse.uwa.edu.au/~pk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files(the "Software"), to deal
 * in the Software without restriction, subject to the following conditions :
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * The Software is provided "as is", without warranty of any kind.
 *
 * --------------------------------------------------------------
 *
 * Copyright(c) 2014 - 2015 Vincent Simonetti
 * Stevens Institute of Technology
 * vsimonet at stevens edu
 *
 * --------------------------------------------------------------
 *
 * June 2003
 * October 2014 - Ported to C/C++ with Armadillo
 */
#define _USE_MATH_DEFINES
#include <morlib.h>
#include <mor.hpp>
#include <math.h>
#include <memory>

using namespace arma;
MORLIB_NAMESPACE_USE;

// Similar copyright to the ransac, fitting, and util
void testfitplane(float outliers, float sigma, float t, bool feedback = false)
{
	// Hard wire some constants - vary these as you wish

	auto npts = 100; // Number of 3D data points

	// Define a plane  ax + by + cz + d = 0
	auto a = 10.0f;
	auto b = -3.0f;
	auto c = 5.0f;
	auto d = 1.0f;

	auto B = fmat();
	B << a << b << c << d << endr;
	B = normalise(B.t());

	auto outsigma = 30 * sigma; // outlying points have a distribution that is 30 times as spread as the inlying points

	auto vpts = static_cast<int>(round((1 - outliers) * npts)); // No of valid points
	auto opts = npts - vpts;                                    // No of outlying points

	// Generate npts points in the plane
	auto X = randu<fmat>(1, npts);
	auto Y = randu<fmat>(1, npts);
	auto Z = (-a * X - b * Y - d) / c;

	fmat XYZ = join_vert(join_vert(X, Y), Z);

	// Add uniform noise of +/-sigma
	XYZ += (2 * randu<fmat>(XYZ.n_rows, XYZ.n_cols) - 1) * sigma;

	// Generate opts random outliers

	auto n = length(XYZ);
	auto ind = random_sample(n, n); // Note: originally randperm, but randomsample performs the same action
	ind = ind.cols(span(0, opts - 1));

	// Add uniform noise of outsigma to the points chosen to be outliers.
	//XYZ.cols(ind) += (2 * randu<fmat>(3, opts) - 1) * outsigma;
	XYZ.cols(ind) += sign(randu<fmat>(3, opts) - 0.5f) % (randu<fmat>(3, opts) + 1.0f) * outsigma; // Note: % -> .*

	// Perform RANSAC fitting of the plane
	fmat P;
	uvec inliers;
	auto Bfitted = ransac_fit_plane(XYZ, t, &P, &inliers, feedback);

	//TODO: probably want to print something out...
}

int main(int argc, char** argv)
{
#if 1
	testfitplane(0.3f, 0.05f, 0.05f);
#else
	printf("Loading depth image\n");
#if 0
	// Heck of a lot faster then the depth frame loading...
	mat_file_t file = mat_file_open("C:\\<NYU data>\\nyu_depth_v2_labeled.mat");
	auto start = counterMS();
	auto depth_frame = mat_file_load_matrix_float(file, "rawDepths", 0); // The last value is the index over time. rawDepth1 = 0, rawDepth2 = 1, rawDepth3 = 2, etc.
	auto loadTime = counterMS() - start;
	mat_file_close(file);
#else
	fmat depth_frame;
	auto start = counterMS();
	// Get the "depth frames" at https://bitbucket.org/jinglebomb/mobile-object-recognition/downloads/DepthFrames.zip
	depth_frame.load("C:\\<some folder>\\rawDepth1.csv", csv_ascii);
	auto loadTime = counterMS() - start;
#endif
	printf("Finished loading depth image. Took %f ms\n", loadTime);

	{
		auto sensorPtr = std::shared_ptr<ISensor>(SensorUtilities::createFakeKinect());
		PointCloud pointCloud(depth_frame, sensorPtr);
		Timer timer(true);
		pointCloud.recomputePointCloud(depth_frame);
		printf("converted to point cloud %f ms\n", timer.getElapsed());
		pointCloud.writeToFile("test.ply");
	}

	printf("Plane hunting...\n");

	fmat filtered_planes;
	uvec plane_inlier_count;
	fspf_params_t params = FSPF_DEFAULT_PARAMS;
	params.plane_normals = &filtered_planes;
	params.plane_inlier_counts = &plane_inlier_count;
	params.feedback = true;
	params.max_filtered_points = depth_frame.n_cols * depth_frame.n_rows;
	params.max_neighborhoods = 20000;
	params.inlier_preallocate = 250000;

	start = counterMS();
	fmat filtered_points = fast_sampling_plane_filtering_depth(depth_frame, float((58.5f * M_PI) / 180.0f), float((45.6f * M_PI) / 180.0f), 60, 0.3f, 80, 0.3f, 0.1f, &params);
	auto runTime = counterMS() - start;

	printf("Finished plane hunting. Took %f ms to find %d planes\n", runTime, filtered_planes.n_rows);

#define OUTPUT_PATH "C:\\<some folder>\\rawDepth1_points.ply"

	printf("Saving filtered points\n");
	fmat colors_f;
	colors_f.load("C:\\<some folder with Depth Frames>\\colors.csv", csv_ascii);
	if (colors_f.n_rows < plane_inlier_count.n_elem)
	{
		printf("Not enough colors for planes, not saving filtered points with colors\n");
		write_ply(OUTPUT_PATH, filtered_points);
	}
	else
	{
		auto colors = cast_to_umat(colors_f);
		umat point_colors(filtered_points.n_rows, 3);

		for (uword i = 0, c = 0; c < plane_inlier_count.n_elem; c++)
		{
			const auto count = plane_inlier_count(c);
			const auto col = colors.row(c);
			for (uword k = 0; k < count && i < filtered_points.n_rows; k++, i++)
			{
				point_colors.row(i) = col;
			}
		}

		write_ply(OUTPUT_PATH, filtered_points, point_colors);
	}
	printf("Done\n");

	system("pause");
#endif
	return 0;
}