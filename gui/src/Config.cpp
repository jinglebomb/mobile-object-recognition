#include "./Config.h"
#include <mor.hpp>

bool Config::init(const std::string& path)
{
    std::ifstream file(path);
    if (file.is_open() == false)
    {
        return false;
    }

    std::string key;
    std::string value;
    while (file.eof() == false)
    {
        std::getline(file, key, '=');
        std::getline(file, value, '\n');

		key = mor::trim(key);
		if (!key.size())
		{
			continue;
		}
        _values[std::string(key)] = std::string(value);
    }
    file.close();
    return true;
}

// static
Config Config::fromMap(const std::map<std::string, std::string>& map)
{
	Config config;
	config._values = map;

	return config;
}

Config Config::fromFile(const std::string& path)
{
    Config config;
    if (config.init(path))
    {
        return config;
    }

    return Config();
}

int Config::getInt(const std::string& key, int def) const
{
	auto it = _values.find(key);
	if (it == _values.end())
	{
		return def;
	}
	return std::stoi(it->second);
}

const std::string& Config::getString(const std::string& key, const std::string& def) const
{
	auto it = _values.find(key);
	if (it == _values.end())
	{
		return def;
	}
	return it->second;
}
