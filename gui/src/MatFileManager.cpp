#include "./MatFileManager.h"
#include <morlib.h>

#include <algorithm>
#include <unordered_set>

MatFileManager::MatFileManager(const Config& config) :
	_file(nullptr),
	_depth(""),
	_acc(""), _accData(), _accCurrent(),
	_names(""), _nameData(), _labels(""), _labelDataCurrent(),
	_filterDepth(FILTER_DISABLED), _activeFilters(),
	_index(0), _indexCount(0), _incIndex(true)
{
	std::string file = config.getString("fileMatSrc");
	if (file.size())
	{
		_file = new Matfile(file);
	}
	_depth = config.getString("fileMatDepthName");
	_acc = config.getString("fileMatAccName");
	_names = config.getString("fileMatLabelReferenceName");
	_labels = config.getString("fileMatLabelName");
}

MatFileManager::~MatFileManager()
{
	delete _file;
	_file = nullptr;
}

int MatFileManager::getElevationAngle() const
{
	if (_accData.n_elem)
	{
		return static_cast<int>(_accCurrent.at(3));
	}
	return 0;
}

bool MatFileManager::getAccelerometerReading(float* values) const
{
	if (_accData.n_elem)
	{
		values[0] = _accCurrent.at(0);
		values[1] = _accCurrent.at(1);
		values[2] = _accCurrent.at(2);
		return true;
	}
	return false;
}

bool MatFileManager::init()
{
	if (_indexCount != 0)
	{
		return true;
	}
	if (_file->getVariableType(_depth) != Matfile_Float)
	{
		return false;
	}
	if (_file->getVariableType(_acc) == Matfile_Float && _file->getDimension(_acc, Matfile_Columns) == 4)
	{
		_accData = _file->loadMatrix<float>(_acc);
	}
	if (_file->getVariableType(_names) == Matfile_StringArray)
	{
		_nameData = _file->loadStringArray(_names);
	}
	if (_file->getVariableType(_labels) != Matfile_U16)
	{
		_labels = "";
	}

	_index = 0;
	if (_file->getDimension(_depth, Matfile_DimensionCount) == 3)
	{
		_indexCount = _file->getDimension(_depth, Matfile_Slices);
	}
	else
	{
		_indexCount = 1;
	}
	return true;
}

bool MatFileManager::startStreaming()
{
	if (_indexCount == 0)
	{
		return false;
	}

	_index = 0;
	return true;
}

void MatFileManager::stopStreaming()
{
}

bool MatFileManager::isConnected() const
{
	return _file->isLoaded();
}

bool MatFileManager::getDepthContents(fmat* depthMatrix, unsigned char* image)
{
	if (_indexCount == 0)
	{
		return false;
	}

	fmat depth = _file->loadMatrix<float>(_depth, _index) * 1000.0f;
	if (!_labels.empty())
	{
		_labelDataCurrent = _file->loadMatrix<u16>(_labels, _index);

		if (_filterDepth != FILTER_DISABLED)
		{
			//XXX is there a better way to do this?
			umat filterInd(depth.n_rows, depth.n_cols, fill::zeros);
			for (u16 label : _activeFilters)
			{
				filterInd = filterInd || (_labelDataCurrent == label);
			}
			if (_filterDepth == FILTER_LABEL)
			{
				// We need the inverse in order to filter everything else... and Armadillo doesn't have an inverse function
				filterInd = filterInd == 0;
			}
			depth(find(filterInd)).zeros();
		}
	}
	if (depthMatrix)
	{
		*depthMatrix = depth;
	}
	if (image)
	{
		// Transpose so it displays properly. If Coordinator::computeFromDepthMatrix changes from KinectManager::DEPTH_WIDTH to KinectManager::DEPTH_HEIGHT, then this can be removed
		depth = depth.t();

		// Find all invalid depth indicies
		const uvec invalidIndicies = find(depth <= 0.0f);

		// Convert to greyscale
		uchar_mat imageMatrix = conv_to<uchar_mat>::from((1.0f - ((depth - DEPTH_MIN) / float(DEPTH_MAX - DEPTH_MIN))) * 256);

		// Set all invalid depths to zero
		imageMatrix(invalidIndicies).zeros();

		// Copy image
		memcpy(image, imageMatrix.memptr(), imageMatrix.n_rows * imageMatrix.n_cols);
	}
	if (_accData.n_elem)
	{
		_accCurrent = _accData.row(_index);
	}

	if (_incIndex)
	{
		_index = (_index + 1) % _indexCount;
	}
	return true;
}

std::vector<std::string> MatFileManager::getCurrentLabels() const
{
	std::vector<std::string> ret;
	if (!_labels.empty())
	{
		auto labelsMem = _labelDataCurrent.memptr();
		std::unordered_set<u16> labelData;
		for (uword i = 0; i < _labelDataCurrent.n_elem; i++)
		{
			// Iterate through all elements of the current data label, if it is labeled (!= 0) and hasn't already been added, then add it
			if (labelsMem[i] && labelData.insert(labelsMem[i]).second)
			{
				// Remember, 0 == unlabeled, so values are otherwise 1-based
				ret.push_back(_nameData[labelsMem[i] - 1]);
			}
		}
	}
	return ret;
}

bool MatFileManager::getLabeledContents(Mat<arma::u16>* labels) const
{
	if (!_labels.empty())
	{
		if (labels)
		{
			*labels = _labelDataCurrent;
		}
		return true;
	}
	return false;
}

arma::u16 MatFileManager::findLabelIndex(const std::string& label) const
{
	auto it = std::find(_nameData.begin(), _nameData.end(), label);
	if (it == _nameData.end())
	{
		return 0;
	}
	return (arma::u16)std::distance(_nameData.begin(), it) + 1;
}

static const std::string labelUnlabeled = "unlabeled";
static const std::string labelInvalid = "invalid";

const std::string& MatFileManager::getLabel(arma::u16 index) const
{
	if (index == 0)
	{
		return labelUnlabeled;
	}
	else if (index > _nameData.size())
	{
		return labelInvalid;
	}
	return _nameData[index - 1];
}

bool MatFileManager::filterDepthByLabels(const std::vector<std::string>& labels, FilterType filterType)
{
	if (labels.size() == 0)
	{
		_filterDepth = FILTER_DISABLED;
		return true;
	}
	else if (filterType == FILTER_LABEL || filterType == FILTER_OUT_LABEL)
	{
		std::unordered_set<u16> labelIndicies;
		for (const std::string& label : labels)
		{
			auto val = findLabelIndex(label);
			if (val)
			{
				labelIndicies.insert(val);
			}
		}
		_filterDepth = labelIndicies.empty() ? FILTER_DISABLED : filterType;
		if (_filterDepth != FILTER_DISABLED)
		{
			_activeFilters.assign(labelIndicies.begin(), labelIndicies.end());
		}
		return _filterDepth != FILTER_DISABLED;
	}
	return false;
}
