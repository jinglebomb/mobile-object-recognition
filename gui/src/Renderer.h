#pragma once

#include <windows.h>
#include <iostream>
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <memory>

#include "./rendering/Program.h"
#include "./rendering/TestRenderable.h"
#include "./rendering/TextureRenderable.h"
#include "./rendering/PointCloudRenderable.h"

class Renderer
{
public:
    enum Mode { Test, Depth, PointCloud, Planes, Quads };

    ~Renderer();

    bool init(int width, int height);

    inline Mode mode() const { return _mode; }
    void setMode(Mode mode);

    void setDepthMap(const GLubyte* data, int width, int height);
    void setPointCloudData(const GLfloat* data, const GLfloat* colors, int count);
    void setPlaneData(const GLfloat* data, const GLfloat* colors, int count);
    void setQuadData(const GLfloat* data, const GLfloat* colors, int count);

	GLFWwindow* window() { return _window; }
	Camera* camera() { return _camera.get(); }
	int getWidth() const;
	int getHeight() const;

    bool windowIsOpen() const;
	void requestWindowClose();

    void renderFrame();

private:
    GLFWwindow* _window;
    Mode _mode;
    std::shared_ptr<Camera> _camera;
    TestRenderable _testRenderable;
    TextureRenderable _depthRenderable;
    PointCloudRenderable _pointCloudRenderable;
    PointCloudRenderable _planeRenderable;
    PointCloudRenderable _quadRenderable;

    bool initRenderables();
};