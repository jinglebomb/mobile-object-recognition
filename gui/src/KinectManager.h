#pragma once

#include <windows.h>
#include <mor.hpp>
#define USE_KINECT
#ifdef USE_KINECT

#include <NuiApi.h>
#include <armadillo>

extern const USHORT DEPTH_MIN;
extern const USHORT DEPTH_MAX;

class KinectManager : public mor::ISensor
{
public:
	static const int DEPTH_WIDTH = 640;
	static const int DEPTH_HEIGHT = 480;
	static const int DEPTH_IMAGE_SIZE = DEPTH_WIDTH * DEPTH_HEIGHT;

	KinectManager();
	~KinectManager();

	inline float getFovY() const { return float((NUI_CAMERA_DEPTH_NOMINAL_VERTICAL_FOV * M_PI) / 180.0f); }
	inline float getFovX() const { return float((NUI_CAMERA_DEPTH_NOMINAL_HORIZONTAL_FOV * M_PI) / 180.0f); }

	inline float getFocalLength() const { return NUI_CAMERA_DEPTH_NOMINAL_FOCAL_LENGTH_IN_PIXELS; }
	inline float getInverseFocalLength() const { return NUI_CAMERA_DEPTH_NOMINAL_INVERSE_FOCAL_LENGTH_IN_PIXELS; }
	int getElevationAngle() const;
	bool getAccelerometerReading(float* values) const;

	inline unsigned int getDepthImageWidth() const { return DEPTH_WIDTH; }
	inline unsigned int getDepthImageHeight() const { return DEPTH_HEIGHT; }
	inline unsigned int getDepthImageBPP() const { return 1; }

	bool init();
	bool startStreaming();
	void stopStreaming();

	bool isConnected() const;

	bool getDepthContents(arma::fmat* depth, unsigned char* image);

private:
	// Handle to the kinect
	INuiSensor* _sensor;
	HANDLE _depthStream;
};

#else

#define NUI_CAMERA_DEPTH_NOMINAL_VERTICAL_FOV 45.6f
#define NUI_CAMERA_DEPTH_NOMINAL_HORIZONTAL_FOV 58.5f
#define NUI_CAMERA_DEPTH_NOMINAL_FOCAL_LENGTH_IN_PIXELS 285.63f
#define NUI_CAMERA_DEPTH_NOMINAL_INVERSE_FOCAL_LENGTH_IN_PIXELS (1.0f / NUI_CAMERA_DEPTH_NOMINAL_FOCAL_LENGTH_IN_PIXELS)

extern const USHORT DEPTH_MIN;
extern const USHORT DEPTH_MAX;

class KinectManager
{
public:
    static const int DEPTH_WIDTH = 640;
    static const int DEPTH_HEIGHT = 480;
    static const int DEPTH_IMAGE_SIZE = DEPTH_WIDTH * DEPTH_HEIGHT;
};

#endif
