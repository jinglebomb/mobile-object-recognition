#pragma once

#include <GL\glew.h>
#include <glm\vec2.hpp>

#include "./IRenderable.h"
#include "./Program.h"

class TextureRenderable : IRenderable
{
public:
    bool init();
    void render();
    void setTexture(const GLubyte* data, int width, int height);

private:
    GLuint _vao;
    Program* _program;
    GLuint _texture;
};