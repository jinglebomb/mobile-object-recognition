#pragma once

#include <cmath>
#include <GL\glew.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

class Camera
{
public:
    Camera();
    void setCenter(float x, float y, float z);
    void setRotation(float rotationX, float rotationY);
    void setStartZoom(float zoom);
    void move(float x, float y, float z);
    void rotate(float rotationX, float rotationY);
    void saveRotate();
    void zoom(float zoom);
    void reset();
    void setPerspective(float fovy, float aspect, float near, float far);
    void setOrtho(float left, float right, float top, float bottom, float near, float far);
    const glm::mat4& matrix();

private:
    glm::vec3 _center;
    glm::vec3 _translation;
    glm::vec2 _startRotation;
    glm::vec2 _rotation;
    float _startZoom;
    float _zoom;
    bool _dirty;

    glm::mat4 _matrix;
    glm::mat4 _projection;
};