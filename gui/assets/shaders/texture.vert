﻿#version 420

in vec2 vPosition;

out vec2 texCoord;

void main() 
{ 
	vec2 normalized = max(vPosition, vec2(0, 0));
	normalized.y = 1 - normalized.y;
	texCoord = normalized;
	gl_Position = vec4(vPosition, 0, 1);
}