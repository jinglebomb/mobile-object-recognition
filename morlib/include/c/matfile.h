/*
 * Copyright(c) 2014 - 2015 Vincent Simonetti
 * Stevens Institute of Technology
 * vsimonet at stevens edu
 */

#ifndef MORLIB_MATFILE_H
#define MORLIB_MATFILE_H

#include <stdmor.h>

MORLIB_NAMESPACE

typedef void* mat_file_t;

MORLIB_API mat_file_t mat_file_open(const char* file);
MORLIB_API void mat_file_close(mat_file_t file);
MORLIB_API bool mat_file_loaded(mat_file_t file);

// Type functions
MORLIB_API bool mat_file_is_float(mat_file_t file, const char* name);
MORLIB_API bool mat_file_is_double(mat_file_t file, const char* name);
MORLIB_API bool mat_file_is_u16(mat_file_t file, const char* name);
MORLIB_API bool mat_file_is_string_array(mat_file_t file, const char* name);

// Information functions
MORLIB_API unsigned int mat_file_get_dimensions(mat_file_t file, const char* name);
MORLIB_API arma::uword mat_file_get_rows(mat_file_t file, const char* name);
MORLIB_API arma::uword mat_file_get_cols(mat_file_t file, const char* name);
MORLIB_API arma::uword mat_file_get_slices(mat_file_t file, const char* name);

// Loading functions
MORLIB_API arma::fmat mat_file_load_matrix_float(mat_file_t file, const char* name, arma::uword slice = ARMA_MAX_UWORD);
MORLIB_API arma::fcube mat_file_load_cube_float(mat_file_t file, const char* name);

MORLIB_API arma::mat mat_file_load_matrix_double(mat_file_t file, const char* name, arma::uword slice = ARMA_MAX_UWORD);
MORLIB_API arma::cube mat_file_load_cube_double(mat_file_t file, const char* name);

MORLIB_API arma::Mat<arma::u16> mat_file_load_matrix_u16(mat_file_t file, const char* name, arma::uword slice = ARMA_MAX_UWORD);
MORLIB_API arma::Cube<arma::u16> mat_file_load_cube_u16(mat_file_t file, const char* name);

// NULL terminated array with standard C strings.
MORLIB_API const char* const* mat_file_load_string_array(mat_file_t file, const char* name);
MORLIB_API void mat_file_free_string_array(const char* const* arr);

//TODO

MORLIB_NAMESPACE_END

#endif
