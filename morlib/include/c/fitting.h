/* 
 * Copyright(c) 2014 - 2015 Vincent Simonetti
 * Stevens Institute of Technology
 * vsimonet at stevens edu
 *
 * --------------------------------------------------------------
 *
 * "fit_plane", "ransac_fit_plane"
 *
 * Copyright(c) 2003-2008 Peter Kovesi
 * School of Computer Science & Software Engineering
 * The University of Western Australia
 * http://www.csse.uwa.edu.au/~pk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files(the "Software"), to deal
 * in the Software without restriction, subject to the following conditions :
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * The Software is provided "as is", without warranty of any kind.
 *
 * --------------------------------------------------------------
 *
 * "prune_set"
 *
 * Copyright (c) 2011-2013 Anders Hast
 * Uppsala University
 * http://www.cb.uu.se/~aht
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * The Software is provided "as is", without warranty of any kind.
 */

#ifndef MORLIB_FITTING_H
#define MORLIB_FITTING_H

#include <armadillo>
#include <stdmor.h>

MORLIB_NAMESPACE

/*
 * Solves coefficients of plane fitted to 3 or more points
 *
 * @param XYZ 3xNpts array of xyz coordinates to fit plane to.
 * @param latent Singular values.
 * @param coeff 3x3 right-singular vectors.
 * @return If the process succeeded or not.
 */
MORLIB_API bool fit_plane_3x3_pca(const arma::fmat& XYZ, arma::fvec& latent, arma::fmat& coeff);

/*
 * Solves coefficients of plane fitted to 3 or more points
 *
 * @param XYZ 3xNpts array of xyz coordinates to fit plane to.
 * @return 3x1 array of plane coefficients in the form b[0]*X + b[1]*Y +b[2]*Z = -d. The magnitude of B is 1. d needs to be calculated on your own.
 */
MORLIB_API arma::fcolvec fit_plane_3x3(const arma::fmat& XYZ);

/*
 * Solves coefficients of plane fitted to 3 or more points
 *
 * @param XYZ 3xNpts array of xyz coordinates to fit plane to.
 * @param U NxN left-singular vectors.
 * @param S Singular values.
 * @param V 4x4 right-singular vectors.
 * @return If the process succeeded or not.
 */
MORLIB_API bool fit_plane_svd(const arma::fmat& XYZ, arma::fmat& U, arma::fvec& S, arma::fmat& V);

/*
 * Solves coefficients of plane fitted to 3 or more points
 *
 * @param XYZ 3xNpts array of xyz coordinates to fit plane to.
 * @return 4x1 array of plane coefficients in the form b[0]*X + b[1]*Y +b[2]*Z + b[3] = 0. The magnitude of B is 1.
 */
MORLIB_API arma::fcolvec fit_plane(const arma::fmat& XYZ);

/*
 * Fits plane to 3D array of points using RANSAC.
 *
 * @param XYZ 3xNpts array of xyz coordinates to fit plane to.
 * @param t The distance threshold between data point and the plane used to decide whether a point is an inlier or not.
 * @param P Returned model of the fit plane.
 * @param inliers Returned inliers to the plane.
 * @param feedback Flag to turn on RANSAC feedback information.
 * @return The three points in the data set that were found to define a plane having the most number of inliers. The three columns of P defining the three points.
 */
MORLIB_API arma::fcolvec ransac_fit_plane(const arma::fmat& XYZ, float t, arma::fmat* P = NULL, arma::uvec* inliers = NULL, bool feedback = false);

/*
 * Try to obtain the best homography from the whole set of points using the already computed homography, which can be computed by any RANSAC alhorithm.
 *
 * @param H The model Homography for x.
 * @param x Data sets to which we are seeking to fit a model H.
 * @param acc  The distance threshold between a data point and the model used to decide whether the point is an inlier or not.
 * @param fittingfn Handle to a function that fits a model to 's' data from 'x'.
 * @param distfn Handle to a function that evaluates the distances from the model to data 'x'.
 * @param putative An array of indices of the elements of 'x' that were the inliers for the pruned set.
 * @param len The length of the putative set.
 * @return The model for the pruned set.
 */
//MORLIB_API arma::fmat prune_set(const arma::fmat& H, const arma::fmat& x, float acc, fitting_func fittingfn, distance_func distfn, arma::umat* putative = NULL, arma::uword* len = NULL);

MORLIB_NAMESPACE_END

#endif
