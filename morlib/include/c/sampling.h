/*
 * Copyright(c) 2014 - 2015 Vincent Simonetti
 * Stevens Institute of Technology
 * vsimonet at stevens edu
 *
 * --------------------------------------------------------------
 *
 * "ransac"
 *
 * Copyright(c) 2003 - 2013 Peter Kovesi
 * Centre for Exploration Targeting
 * The University of Western Australia
 * peter.kovesi at uwa edu au
 * http://www.csse.uwa.edu.au/~pk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files(the "Software"), to deal
 * in the Software without restriction, subject to the following conditions :
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * The Software is provided "as is", without warranty of any kind.
 *
 * --------------------------------------------------------------
 *
 * "optimal_ransac"
 *
 * From: http://www.cb.uu.se/~aht/code.html
 *
 * --------------------------------------------------------------
 *
 * "fast_sampling_plane_filtering"
 *
 * From: http://www.ri.cmu.edu/pub_files/2012/5/icra2012_kinectLocalization.pdf
 */

#ifndef MORLIB_SAMPLING_H
#define MORLIB_SAMPLING_H

#include <armadillo>
#include <stdmor.h>

#include <functional>

MORLIB_NAMESPACE

typedef bool (*degenerate_func)(const arma::fmat& M);
typedef std::function<bool(const arma::fmat& plane_points, float plane_size, arma::uword width, arma::uword height, arma::uword* bound_width, arma::uword* bound_height)> plane_bound_calc_func;

/*
 * Robustly fits a model to data with the RANSAC algorithm
 *
 * @param x Data sets to which we are seeking to fit a model M.
 * @param fittingfn Handle to a function that fits a model to 's' data from 'x'.
 * @param distfn Handle to a function that evaluates the distances from the model to data 'x'.
 * @param degenfn Handle to a function that determines whether a set of datapoints will produce a degenerate model.
 * @param s The minimum number of samples from 'x' required by fittingfn to fit a model.
 * @param t The distance threshold between a data point and the model used to decide whether the point is an inlier or not.
 * @param inliers An array of indices of the elements of 'x' that were the inliers for the best model.
 * @param feedback If set to true the trial count and the estimated total number of trials required is printed out at each step.
 * @param max_data_trials Maximum number of attempts to select a non-degenerate data set.
 * @param max_trials Maximum number of iterations.
 * @param p The desired probability of choosing at least one sample free from outliers.
 * @return The model having the greatest number of inliers. Result is empty if no solution was found.
 */
MORLIB_API arma::fmat ransac(const arma::fmat& x, fitting_func fittingfn, distance_func distfn, degenerate_func degenfn, arma::uword s, float t,
	arma::uvec* inliers = NULL, bool feedback = false, unsigned int max_data_trials = 100, unsigned int max_trials = 1000, float p = 0.99f);

/*
 * Robustly fits a model to data with the Optimal RANSAC algorithm.
 *
 * @param x Data sets to which we are seeking to fit a model M.
 * @param fittingfn Handle to a function that fits a model to 's' data from 'x'.
 * @param distfn Handle to a function that evaluates the distances from the model to data 'x'.
 * @param s The minimum number of samples from 'x' required by fittingfn to fit a model.
 * @param t The distance threshold between a data point and the model used to decide whether the point is an inlier or not.
 * @param acc The same as above but is used to prune the set. If 'acc < t' then pruning is performed.
 * @param inliers An array of indices of the elements of 'x' that were the inliers for the best model.
 * @param iter Number of iterations. This is measuered in terms of ordinary RANSAC iterations so it can be compared to RANSAC.
 * @param max_data_trials Maximum number of attempts to select a non-degenerate data set.
 * @param max_trials Maximum number of iterations.
 * @param maxit Do not repeat rescoring more than maxit times.
 * @param nrtrials Defines how many resamplings shall be done.
 * @param low How many inliers are required before optimization is done.
 * @param ner Number of equal sets required before stopping.
 * @return The model having the greatest number of inliers.
 */
//MORLIB_API arma::fmat optimal_ransac(const arma::fmat& x, fitting_func fittingfn, distance_func distfn, arma::uword s, float t, float acc,
//	arma::umat* inliers = NULL, unsigned int* iter = NULL, unsigned int max_data_trials = 100, unsigned int max_trials = 10000,
//	unsigned int maxit = 20, unsigned int nrtrials = 8, arma::uword low = 5, unsigned int ner = 1);

/*
 * Additional parameters for fast sampling plane filtering functions.
 */
typedef struct fspf_params_s
{
	arma::fmat* plane_normals;			// Normals to planes.
	arma::umat* inlier_indices;			// Absolute indicies for inliers within the source image or point cloud. Note: while the matrix is a uniform size, each row is only as long as plane_inlier_counts's count for that row.
	arma::uvec* plane_inlier_counts;	// The number of inliers associated for each plane, in the same order as the planes themselves.
	arma::fmat* outlier_points;			// Outlier points.
	arma::fmat* aa_extends;				// Axis - aligned extents of each plane's inliers. Columns 0-2 are min<x,y,z>, while columns 3-5 are max<x,y,z>.
	arma::fmat* obb_stats;				// Oriented bounding-box of each plane's inliers. Columns 0-2 are center<x,y,z>, columns 3-5 are width, height, depth, columns 6-8, 9-11, 12-14 are each of it's axis<x,y,z>.
	arma::fmat* obb_corners;			// Oriented bounding-box of each plane's inliers. Columns are <x,y,z> of all 8 corners (0-2 corner 1, 3-5 corner 2, etc.), in clockwise/top-bottom order.
	bool is_obb_corners_box;			// If obb_corners is set, should a full OBB be returned, or just the plane corners.
	bool feedback;						// If set to true the trial count and the estimated total number of trials required is printed out at each step.
	arma::uword max_filtered_points;	// Maximum total number of filtered points.
	unsigned int max_neighborhoods;		// Maximum number of neighborhoods to sample.
	arma::uword inlier_preallocate;		// The number of inlier rows to pre - allocate for execution.
	bool reset_on_fail;					// If not enough data / invalid data exists, should the sample count ignore that sample run or count it anyway ?
	bool remove_duplicates;				// Remove duplicate points from inliers, plane normals, and outliers.This is ignored if plane_inlier_counts is not NULL.
	arma::vec* performance;				// Performance results (everything in MS): [0] overall time, [1] sampling loop, [2] AA-extends, [3] OBB-stats, [4] OBB-corners, [5] inlier indicies, [6] outliers
} fspf_params_t;

#define FSPF_DEFAULT_PARAMS { \
	NULL,	/* plane_normals */ \
	NULL,	/* inlier indicies */ \
	NULL,	/* plane_inlier_counts */ \
	NULL,	/* outlier_points */ \
	NULL,	/* aa_extends */ \
	NULL,	/* obb_stats */ \
	NULL,	/* obb_corners */ \
	false,	/* is_obb_corners_box */ \
	false,	/* feedback */ \
	2000,	/* max_filtered_points */ \
	20000,	/* max_neighborhoods */ \
	300000,	/* inlier_preallocate */ \
	false,	/* reset_on_fail */ \
	false,	/* remove_duplicates */ \
	NULL	/* performance */ \
	}

/*
 * Reduce the volume of a 3D point cloud from a depth image.
 *
 * This function reduces the volume of the 3D point cloud by sampling points from the depth image, and classifying local
 * grouped sets of points as belonging to planes in 3D (the "plane filtered" points) or points that do not correspond to planes
 * within a specified error margin (the "outlier" points).
 * Source: http://www.ri.cmu.edu/pub_files/2012/5/icra2012_kinectLocalization.pdf
 *
 * Random sampling (POSIX rand) occurs within this function, so if a seed is not set it may produce different results for the same
 * depth image.
 *
 * @param i The depth image to sample.
 * @param fh Horizontal field of view.
 * @param fv Vertical field of view.
 * @param neighborhood_range Neighborhood for global samples (in pixels).
 * @param plane_size Plane size in world space for local samples.
 * @param local_samples Number of local samples. (currently unused)
 * @param s Precent of samples that are inliers to accept the sample.
 * @param t The distance threshold between a data point and the model used to decide whether the point is an inlier or not.
 * @param params Additional parameters.
 * @return Filtered inlier points.
 */
MORLIB_API arma::fmat fast_sampling_plane_filtering_depth(const arma::fmat& i, float fh, float fv, arma::sword neighborhood_range, float plane_size, arma::uword local_samples, float s, float t,
	fspf_params_t* params = NULL);

/*
 * Reduce the volume of a 3D point cloud.
 *
 * This function reduces the volume of the 3D point cloud by sampling points from a windowed subset, and classifying local
 * grouped sets of points as belonging to planes in 3D (the "plane filtered" points) or points that do not correspond to planes
 * within a specified error margin (the "outlier" points).
 * Source: http://www.ri.cmu.edu/pub_files/2012/5/icra2012_kinectLocalization.pdf
 *
 * Random sampling (POSIX rand) occurs within this function, so if a seed is not set it may produce different results for the same
 * point cloud.
 *
 * @param XYZ The Nx3 point cloud to sample. It is expected that this was generated column by column, such as what depth_image_to_point_cloud creates.
 * @param width The width of the original source depth image. Used for window calculation.
 * @param height The height of the source original depth image. Used for window calculation.
 * @param neighborhood_range Neighborhood for global samples (in pixels).
 * @param plane_size Plane size in world space for local samples.
 * @param local_samples Number of local samples. (currently unused)
 * @param s Precent of samples that are inliers to accept the sample.
 * @param t The distance threshold between a data point and the model used to decide whether the point is an inlier or not.
 * @param bound_calc Window calculator function. Determines the area to actual evaluate for inliers and planes.
 * @param params Additional parameters.
 * @return Filtered inlier points.
 */
MORLIB_API arma::fmat fast_sampling_plane_filtering_points(const arma::fmat& XYZ, arma::uword width, arma::uword height, 
	arma::sword neighborhood_range, float plane_size, arma::uword local_samples, float s, float t, plane_bound_calc_func bound_calc, 
	fspf_params_t* params = NULL);

MORLIB_NAMESPACE_END

#endif