/*
 * Copyright(c) 2014 - 2015 Krzysztof Jordan
 * Stevens Institute of Technology
 * kjordan1 at stevens edu
 */

#pragma once

#include <stdmor.h>
#include <armadillo>

MORLIB_NAMESPACE

class ISensor
{
public:
	// Radians
	virtual float getFovY() const = 0;
	virtual float getFovX() const = 0;

	virtual float getFocalLength() const = 0;
	virtual float getInverseFocalLength() const = 0;

	virtual int getElevationAngle() const = 0;
	// 'values' should be at least 4 elements in size
	virtual bool getAccelerometerReading(float* values) const = 0;

	virtual unsigned int getDepthImageWidth() const = 0;
	virtual unsigned int getDepthImageHeight() const = 0;
	virtual unsigned int getDepthImageBPP() const = 0;

	virtual bool init() = 0;
	virtual bool startStreaming() = 0;
	virtual void stopStreaming() = 0;

	virtual bool isConnected() const = 0;

	// 'depth' and 'image' can be null
	virtual bool getDepthContents(arma::fmat* depth, unsigned char* image) = 0;
};

MORLIB_NAMESPACE_END
