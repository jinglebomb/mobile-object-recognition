/*
 * Copyright(c) 2014 - 2015 Vincent Simonetti
 * Stevens Institute of Technology
 * vsimonet at stevens edu
 */

#pragma once

#include <armadillo>
#include <string>

#include <stdmor.h>

MORLIB_NAMESPACE

enum MatfileType
{
	Matfile_Unknown,

	Matfile_U16,
	Matfile_Float,
	Matfile_Double,
	Matfile_StringArray
};

enum MatfileDimension
{
	Matfile_DimensionCount,

	Matfile_Rows,
	Matfile_Columns,
	Matfile_Slices
};

class Matfile
{
public:
	Matfile(const std::string& file);

	~Matfile();

	bool isLoaded() const;

	MatfileType getVariableType(const std::string& varName) const;
	arma::uword getDimension(const std::string& varName, MatfileDimension dim) const;

	template<typename eT>
	arma::Mat<eT> loadMatrix(const std::string& varName, arma::uword slice = ARMA_MAX_UWORD) const;

	template<typename eT>
	arma::Cube<eT> loadCube(const std::string& varName) const;

	std::vector<std::string> loadStringArray(const std::string& varName) const;

	//TODO

private:
	void* mFile;

	Matfile(const Matfile&){}
};

MORLIB_NAMESPACE_END

#include "impl/Matfile_impl.hpp"
