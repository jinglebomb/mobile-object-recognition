/*
 * Copyright(c) 2014 - 2015 Krzysztof Jordan
 * Stevens Institute of Technology
 * kjordan1 at stevens edu
 */

#pragma once

#include "stdmor.h"

#include <armadillo>

#include <flann/flann.hpp>
#include <flann/io/hdf5.h>

MORLIB_NAMESPACE

class FeatureMatcher
{
public:
	FeatureMatcher(const arma::fmat& features, const arma::Row<arma::u16>& labels);
	~FeatureMatcher();

	arma::u16 match(const flann::Matrix<float>& query);

private:
	flann::Index<flann::L2<float>>* _index;
	flann::Matrix<float> _features;	
	arma::Row<arma::u16> _labels;

	flann::Matrix<int> _indices;
	flann::Matrix<float> _distances;

	size_t _featureSize;
};

MORLIB_NAMESPACE_END

#include "impl/FeatureMatcher_impl.hpp"
