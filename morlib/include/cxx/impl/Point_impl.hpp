#include "../Point.hpp"
#include "../ISensor.hpp"

using namespace arma;
MORLIB_NAMESPACE_USE;

inline Point::Point(float value) :
	x(value),
	y(value),
	z(value)
{
}

inline Point::Point(float x, float y, float z) :
	x(x), 
	y(y),
	z(z)
{
}

inline Point::Point(const subview_row<float>& vec) :
	x(vec[0]),
	y(vec[1]),
	z(vec[2])
{
}

inline Point::Point(const ISensor* sensor, uword x, uword y, uword width, uword height, float depth) :
	x(depth * sensor->getInverseFocalLength() * y),
	y(depth * sensor->getInverseFocalLength() * x),
	z(depth)
{
}

inline Point::Point(const ISensor* sensor, uword x, uword y, uword width, uword height, float depth, float th, float tv) :
	x(depth * (x / (width - 1.0f) - 0.5f) * th),
	y(depth * (y / (height - 1.0f) - 0.5f) * tv),
	z(depth)
{
}
 