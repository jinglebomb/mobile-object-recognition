#include "../PointCloud.hpp"
#include "../ISensor.hpp"
#include "../Utilities.hpp"

#include "../../c/fitting.h"
#include "../../c/sampling.h"
#include "../../c/util.h"

using namespace arma;
MORLIB_NAMESPACE_USE;

inline PointCloud::PointCloud(const arma::fmat& depthMatrix, const std::shared_ptr<ISensor>& sensorPtr, bool computeAtStart) :
	_imageSeed(0),
	_points(depthMatrix.n_elem),
	_width(depthMatrix.n_cols),
	_height(depthMatrix.n_rows),
	_sensorPtr(sensorPtr),
	_min(),
	_max()
{
	if (computeAtStart)
	{
		recomputePointCloud(depthMatrix);
	}
}

inline void PointCloud::recomputePointCloud(const arma::fmat& depthMatrix)
{
	arma_debug_check(((_width != depthMatrix.n_cols) || (_height != depthMatrix.n_rows)), "recomputePointCloud: depthMatrix is not the same size as PointCloud");

	_imageSeed = mor::generate_seed(depthMatrix);

    auto th = tanf(_sensorPtr->getFovX());
    auto tv = tanf(_sensorPtr->getFovY());

	_min = Point(FLT_MAX);
	_max = Point(FLT_MIN);

	for (auto xi = uword(0); xi < _width; xi++)
	{
		for (auto yi = uword(0); yi < _height; yi++)
		{
			auto depth = depthMatrix.at(yi, xi);
			auto p = Point(_sensorPtr.get(), yi, xi, _width, _height, depth, th, tv);

			// Calculate bounds of point cloud
			if (_min.x > p.x) { _min.x = p.x; }
			else if (_max.x < p.x) { _max.x = p.x; }
			if (_min.y > p.y) { _min.y = p.y; }
			else if (_max.y < p.y) { _max.y = p.y; }
			if (_min.z > p.z) { _min.z = p.z; }
			else if (_max.z < p.z) { _max.z = p.z; }

			_points[getIndex(xi, yi)] = p;
		}
	}
}

inline void PointCloud::writeToFile(const std::string& name) const
{
	std::ofstream file(name);

	file << "ply" << std::endl
		<< "format ascii 1.0" << std::endl
		<< "element vertex " << _points.size() << std::endl
		<< "property float32 x" << std::endl
		<< "property float32 y" << std::endl
		<< "property float32 z" << std::endl
		<< "end_header" << std::endl;

	for (auto i = uword(0); i < _points.size(); i++)
	{
		auto point = _points[i];
		file << point.x << " " << point.y << " " << point.z << std::endl;
	}

	file.flush();
	file.close();
}
inline const Point& PointCloud::getPoint(int index) const
{
	return _points[index];
}

inline const std::vector<Point>& PointCloud::getPoints() const
{
	return _points;
}

inline arma::uword PointCloud::getWidth() const
{
	return _width;
}

inline arma::uword PointCloud::getHeight() const
{
	return _height;
}

inline Point PointCloud::getCenter() const
{
	return Point((_max.x - _min.x) / 2.0f,
		(_max.y - _min.y) / 2.0f,
		(_max.z - _min.z) / 2.0f);
}

inline const Point& PointCloud::getMinBound() const
{
	return _min;
}

inline const Point& PointCloud::getMaxBound() const
{
	return _max;
}

inline int PointCloud::getIndex(int col, int row) const
{
	return row + col * _height;
}

inline Plane PointCloud::fitPlane(float distanceThreshold) const
{
	if (_points.size() < 3)
	{
		return Plane(0, 0, 0, 0);
	}
	const fmat XYZ = create_fmat_from_vector(_points, 3, static_cast<uword>(_points.size()));

	srand(_imageSeed);
	const auto result = ransac_fit_plane(XYZ, distanceThreshold);
	return Plane(result(0), result(1), result(2), result(3));
}

inline std::vector<PlaneFilteringResult> PointCloud::planeFiltering(sword neighborhood_range, float plane_size, float s, float t, 
	const PlaneFilteringParams& inParams) const
{
	if (_points.size() < 3)
	{
		return std::vector<PlaneFilteringResult>();
	}

	Timer perfTimer(true);

	auto fov_h = tanf(_sensorPtr->getFovX());
	auto fov_v = tanf(_sensorPtr->getFovY());
	auto bound_handler = [fov_h, fov_v](const fmat& plane_points, float plane_size, uword width, uword height, uword* bound_width, uword* bound_height)
	{
		auto z = (plane_points.col(0)[2] + plane_points.col(1)[2] + plane_points.col(2)[2]) / 3.0f;
		*bound_width = fast_round_uword(width * (plane_size / z) * fov_h);
		*bound_height = fast_round_uword(height * (plane_size / z) * fov_v);
		return true;
	};
	const fmat XYZ = create_fmat_from_vector(_points, 3, static_cast<uword>(_points.size()));

	srand(_imageSeed);

	fmat filtered_planes;
	uvec filtered_points_count;
	fmat obb_stats;
	fmat obb_corners;
	fspf_params_t params = FSPF_DEFAULT_PARAMS;
	params.plane_normals = inParams.rawPlaneNormals ? inParams.rawPlaneNormals : &filtered_planes;
	params.inlier_indices = inParams.rawInlierIndicies;
	params.plane_inlier_counts = inParams.rawInlierCount ? inParams.rawInlierCount : &filtered_points_count;
	params.obb_stats = &obb_stats;
	params.obb_corners = &obb_corners;
	params.performance = inParams.performanceResults ? inParams.performanceResults : nullptr;
	params.max_filtered_points = inParams.maxFilteredPoints;
	params.max_neighborhoods = inParams.maxNeighborhoods;
	params.inlier_preallocate = inParams.inlierPreallocate;

	const fmat filtered_points = fast_sampling_plane_filtering_points(XYZ, _width, _height, neighborhood_range, plane_size, 0, s, t, bound_handler, &params);

	uword offset = 0;
	uword point_count_index = 0;
	std::vector<PlaneFilteringResult> results;
	if (filtered_points.n_rows > 0)
	{
		const auto plane = params.plane_normals->row(point_count_index);
		const auto obb_stat = obb_stats.row(point_count_index);
		const auto obb_corner = obb_corners.row(point_count_index);
		results.push_back({
			// Normal
#if defined(PLANE_FILTERING_FAST_PLANE_NORMAL_CALCULATION) || !defined(PLANE_FILTERING_4x4_SVD)
			Plane(plane[0], plane[1], plane[2], 0.0f),
#else
			Plane(plane[0], plane[1], plane[2], plane[3]),
#endif
			// Bounds
			{ Point(obb_corner.subvec(0, 2)), Point(obb_corner.subvec(3, 5)), Point(obb_corner.subvec(6, 8)), Point(obb_corner.subvec(9, 11)) },
			std::vector<Point>(),			// Inliers
			Point(obb_stat.subvec(0, 2)),	// Center
			obb_stat(3),					// Width
			obb_stat(4)						// Height
		});
	}
	for (uword i = 0; i < filtered_points.n_rows; i++)
	{
		if ((i - offset) >= params.plane_inlier_counts->at(point_count_index))
		{
			offset += params.plane_inlier_counts->at(point_count_index++);

			const auto plane = params.plane_normals->row(point_count_index);
			const auto obb_stat = obb_stats.row(point_count_index);
			const auto obb_corner = obb_corners.row(point_count_index);
			results.push_back({
				// Normal
#if defined(PLANE_FILTERING_FAST_PLANE_NORMAL_CALCULATION) || !defined(PLANE_FILTERING_4x4_SVD)
				Plane(plane[0], plane[1], plane[2], 0.0f),
#else
				Plane(plane[0], plane[1], plane[2], plane[3]),
#endif
				// Bounds
				{ Point(obb_corner.subvec(0, 2)), Point(obb_corner.subvec(3, 5)), Point(obb_corner.subvec(6, 8)), Point(obb_corner.subvec(9, 11)) },
				std::vector<Point>(),			// Inliers
				Point(obb_stat.subvec(0, 2)),	// Center
				obb_stat(3),					// Width
				obb_stat(4)						// Height
			});
		}
		const auto point = filtered_points.row(i);
		PlaneFilteringResult& result = results[point_count_index];
		result.inliers.push_back(Point(point[0], point[1], point[2]));
#if defined(PLANE_FILTERING_FAST_PLANE_NORMAL_CALCULATION) || !defined(PLANE_FILTERING_4x4_SVD)
		if ((i - offset) == 0)
		{
			// First of the results. Calculate 'd' for the plane
			result.plane.d = -(result.plane.a * point[0] + result.plane.b * point[1] + result.plane.c * point[2]);
		}
#endif
	}

	if (inParams.performanceResults)
	{
		auto perfResult = perfTimer.getElapsed();
		inParams.performanceResults->insert_rows(0, 1);
		inParams.performanceResults->at(0) = perfResult;
	}

	return results;
}
