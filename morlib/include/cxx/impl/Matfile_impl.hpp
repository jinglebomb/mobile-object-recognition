#include "../Matfile.hpp"
#include "../../c/matfile.h"

using namespace arma;
MORLIB_NAMESPACE_USE;

inline Matfile::Matfile(const std::string& file)
	: mFile(mat_file_open(file.c_str()))
{
}

inline Matfile::~Matfile()
{
	mat_file_close(mFile);
}

inline bool Matfile::isLoaded() const
{
	return mat_file_loaded(mFile);
}

inline MatfileType Matfile::getVariableType(const std::string& name) const
{
	const char* str = name.c_str();

	if (mat_file_is_float(mFile, str))
	{
		return Matfile_Float;
	}
	else if (mat_file_is_double(mFile, str))
	{
		return Matfile_Double;
	}
	else if (mat_file_is_string_array(mFile, str))
	{
		return Matfile_StringArray;
	}
	else if (mat_file_is_u16(mFile, str))
	{
		return Matfile_U16;
	}

	return Matfile_Unknown;
}

inline uword Matfile::getDimension(const std::string& name, MatfileDimension dim) const
{
	const char* str = name.c_str();

	switch (dim)
	{
	case Matfile_DimensionCount:
		return mat_file_get_dimensions(mFile, str);
	case Matfile_Rows:
		return mat_file_get_rows(mFile, str);
	case Matfile_Columns:
		return mat_file_get_cols(mFile, str);
	case Matfile_Slices:
		return mat_file_get_slices(mFile, str);
	}
	return 0;
}

template<typename eT>
inline Mat<eT> Matfile::loadMatrix(const std::string&, uword slice) const
{
	return Mat<eT>();
}

template<>
inline fmat Matfile::loadMatrix<float>(const std::string& name, uword slice) const
{
	return mat_file_load_matrix_float(mFile, name.c_str(), slice);
}

template<>
inline mat Matfile::loadMatrix<double>(const std::string& name, uword slice) const
{
	return mat_file_load_matrix_double(mFile, name.c_str(), slice);
}

template<>
inline Mat<u16> Matfile::loadMatrix<u16>(const std::string& name, uword slice) const
{
	return mat_file_load_matrix_u16(mFile, name.c_str(), slice);
}

template<typename eT>
inline Cube<eT> Matfile::loadCube(const std::string&) const
{
	return Cube<eT>();
}

template<>
inline fcube Matfile::loadCube<float>(const std::string& name) const
{
	return mat_file_load_cube_float(mFile, name.c_str());
}

template<>
inline cube Matfile::loadCube<double>(const std::string& name) const
{
	return mat_file_load_cube_double(mFile, name.c_str());
}

template<>
inline Cube<u16> Matfile::loadCube<u16>(const std::string& name) const
{
	return mat_file_load_cube_u16(mFile, name.c_str());
}

inline std::vector<std::string> Matfile::loadStringArray(const std::string& name) const
{
	std::vector<std::string> ret;
	const char* const* strs = mat_file_load_string_array(mFile, name.c_str());
	if (strs)
	{
		for (arma::uword i = 0; strs[i]; i++)
		{
			ret.push_back(std::string(strs[i]));
		}
		mat_file_free_string_array(strs);
	}
	return ret;
}
