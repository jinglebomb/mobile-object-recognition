#include "../FeatureMatcher.hpp"
#include "../Utilities.hpp"

using namespace arma;
MORLIB_NAMESPACE_USE;

inline FeatureMatcher::FeatureMatcher(const fmat& features, const Row<u16>& labels) :
	_labels(labels)
{
	_featureSize = static_cast<size_t>(features.n_rows);
	_features = MatrixHelper::convertToFlann(features);
	_index = new flann::Index<flann::L2<float>>(_features, flann::KDTreeIndexParams(4));
	_index->buildIndex();
	_indices = MatrixHelper::createFlann<int>(1, _featureSize);
	_distances = MatrixHelper::createFlann<float>(1, _featureSize);
}

inline FeatureMatcher::~FeatureMatcher()
{
	delete _index;
	MatrixHelper::cleanupFlann(_features);
	MatrixHelper::cleanupFlann(_indices);
	MatrixHelper::cleanupFlann(_distances);
}

inline u16 FeatureMatcher::match(const flann::Matrix<float>& query)
{
	_index->knnSearch(query, _indices, _distances, 1, flann::SearchParams(128));
	return _labels.at((uword)_indices[0]);
}
