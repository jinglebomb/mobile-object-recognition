#include "../Utilities.hpp"
#include "../ISensor.hpp"
#include "../../c/util.h"

#define _USE_MATH_DEFINES
#include <math.h>

using namespace arma;
MORLIB_NAMESPACE_USE;

inline Timer::Timer(bool startTimer) : mTime(0.0)
{
	if (startTimer)
	{
		start();
	}
}

inline void Timer::start()
{
	mTime = counterMS();
}

inline double Timer::getElapsed() const
{
	return counterMS() - mTime;
}

class SimpleSensor : public ISensor
{
public:
	SimpleSensor(float fovXdeg, float fovYdeg, float focalLength, unsigned int depthWidth, unsigned int depthHeight)
	{
		_fovX = float((fovXdeg * M_PI) / 180.0f);
		_fovY = float((fovYdeg * M_PI) / 180.0f);
		_focalLength = focalLength;
		_invFocalLength = 1.0f / _focalLength;
		_dWidth = depthWidth;
		_dHeight = depthHeight;
	}

	float getFovY() const { return _fovY; }
	float getFovX() const { return _fovX; }

	float getFocalLength() const { return _focalLength; }
	float getInverseFocalLength() const { return _invFocalLength; }
	int getElevationAngle() const { return 0; }
	bool getAccelerometerReading(float* values) const { return false; }

	unsigned int getDepthImageWidth() const { return _dWidth; }
	unsigned int getDepthImageHeight() const { return _dHeight; }
	unsigned int getDepthImageBPP() const { return 1; }

	bool init() { return true; }
	bool startStreaming() { return false; }
	void stopStreaming() {}

	bool isConnected() const { return true; }

	bool getDepthContents(fmat* depth, unsigned char* image) { return false; }
private:
	float _fovX;
	float _fovY;
	float _focalLength;
	float _invFocalLength;
	unsigned int _dWidth;
	unsigned int _dHeight;
};

inline ISensor* SensorUtilities::createSimpleSensor(float fovXdeg, float fovYdeg, float focalLength, unsigned int depthWidth, unsigned int depthHeight)
{
	return new SimpleSensor(fovXdeg, fovYdeg, focalLength, depthWidth, depthHeight);
}

inline ivec MORLIB_NAMESPACE_PREFIX categorizePlanes(const fmat& planeNormals, const uvec& inlierCounts, const fvec3& up)
{
	arma::ivec categories(inlierCounts.n_elem);

	sword groundIndex = -1;
	u32 groundCount;

	for (uword i = 0; i < inlierCounts.n_elem; i++)
	{
		frowvec3 normal = normalise(planeNormals.row(i));

		auto dotProduct = dot(normal, up);

		if (fabs(dotProduct) <= 0.5f)
		{
			if (groundIndex == -1 || groundCount < inlierCounts.at(i))
			{
				groundIndex = static_cast<sword>(i);
				groundCount = inlierCounts.at(i);
			}

			categories.at(i) = PlaneType::SUPPORT;
		}
		else
		{
			categories.at(i) = PlaneType::NON_SUPPORT;
		}
	}

	if (groundIndex != -1)
	{
		categories.at(groundIndex) |= PlaneType::GROUND;
	}

	return categories;
}

// MatrixHelper

template<typename T>
inline flann::Matrix<T> MatrixHelper::createFlann(size_t rows, size_t cols)
{
	return flann::Matrix<T>(new T[rows * cols], rows, cols);
}

template<typename T>
inline void MatrixHelper::cleanupFlann(flann::Matrix<T>& matrix)
{
	delete[] matrix.ptr();
}

template<typename T>
inline flann::Matrix<T> MatrixHelper::convertToFlann(const Mat<T>& matrix)
{
	T* data = new T[matrix.n_elem];
	memcpy(data, matrix.memptr(), sizeof(T) * matrix.n_elem);
	return flann::Matrix<T>(data, static_cast<size_t>(matrix.n_rows), static_cast<size_t>(matrix.n_cols));
}
