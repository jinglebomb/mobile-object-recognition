/*
 * Copyright(c) 2014 - 2015 Vincent Simonetti
 * Stevens Institute of Technology
 * vsimonet at stevens edu
 */

#pragma once

#include <stdmor.h>
#include <cmath>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

#include <flann/flann.hpp>
#include <flann/io/hdf5.h>

MORLIB_NAMESPACE

class Timer
{
public:
	Timer(bool start = false);

	void start();
	double getElapsed() const;

private:
	double mTime;
};

class ISensor;
class SensorUtilities
{
public:
	static ISensor* createSimpleSensor(float fovXdeg, float fovYdeg, float focalLength, unsigned int depthWidth = 0, unsigned int depthHeight = 0);
	static ISensor* createFakeKinect() { return createSimpleSensor(58.5f, 45.6f, 285.63f, 640, 480); }
};

enum PlaneType
{
	GROUND = 1 << 0,
	SUPPORT = 1 << 1,
	NON_SUPPORT = 1 << 2
};

//XXX this needs to go somewhere better
arma::ivec categorizePlanes(const arma::fmat& planeNormals, const arma::uvec& inlierCounts, const arma::fvec3& up);

// String functions from http://stackoverflow.com/a/217605
static inline std::string& trim_front(std::string& s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

static inline std::string& trim_back(std::string& s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

static inline std::string& trim(std::string& s) {
	return trim_front(trim_back(s));
}

class MatrixHelper
{
public:
	template<typename T>
	static flann::Matrix<T> createFlann(size_t rows, size_t cols);
	template<typename T>
	static void cleanupFlann(flann::Matrix<T>& matrix);
	template<typename T>
	static flann::Matrix<T> convertToFlann(const arma::Mat<T>& matrix);

	//TODO
};

//TODO

MORLIB_NAMESPACE_END

#include "impl/Utilities_impl.hpp"
