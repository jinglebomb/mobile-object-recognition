/*
 * Copyright(c) 2014 - 2015 Krzysztof Jordan
 * Stevens Institute of Technology
 * kjordan1 at stevens edu
 */

#pragma once

#include <armadillo>
#include <stdmor.h>
#include "Utilities.hpp"

#include <algorithm>
#include <queue>
#include <utility>

MORLIB_NAMESPACE

class World 
{
public:
	World(const arma::fvec3& up) :
		_up(up)
	{
		_forwards.at(2) = 1;
		_left = arma::cross(_forwards, _up);
	}

	World(const arma::fmat& planeNormals, const arma::ivec planeType, float threshold = 0.1f)
	{
		for (int i = 0; i < planeNormals.n_rows; i++)
		{
			if (planeType(i) && PlaneType::GROUND)
			{
				arma::fvec3 normal = arma::normalise(planeNormals.row(i));

				auto dotProduct = arma::dot(normal, arma::fvec3({ 0, 1, 0 }));
				
				_up = (dotProduct < 0) ? -normal : normal;
				break;
			}
		}

		// compute the other directions
		_forwards.at(2) = 1;
		_left = arma::cross(_forwards, _up);
	}

	inline arma::fvec3 down() const
	{
		return -1 * _up;
	}

	inline const arma::fvec3& up() const
	{
		return _up;
	}

	inline const arma::fvec3& left() const
	{
		return _left;
	}

	inline arma::fvec3 right() const
	{
		return -1 * _left;
	}

	inline const arma::fvec3& forwards() const
	{
		return _forwards;
	}

	inline arma::fvec3 backwards() const
	{
		return -1 * _forwards;
	}

private:
	arma::fvec3 _up;
	arma::fvec3 _left;
	arma::fvec3 _forwards;
};

MORLIB_NAMESPACE_END
