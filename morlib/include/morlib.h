/*
 * Copyright(c) 2014 - 2015 Vincent Simonetti
 * Stevens Institute of Technology
 * vsimonet at stevens edu
 */

#ifndef MORLIB_H
#define MORLIB_H

#include "c/util.h"
#include "c/sampling.h"
#include "c/fitting.h"
#include "c/matfile.h"

#endif
