/*
* Copyright(c) 2014 - 2015 Vincent Simonetti
* Stevens Institute of Technology
* vsimonet at stevens edu
*/

#include <armadillo>
#include <matio.h>

#include <c/matfile.h>
#include <c/util.h>

#include <string>
#include <map>

using namespace arma;
MORLIB_NAMESPACE_USE;

typedef struct mat_file_internal_s
{
	mat_t* matfp;
	std::map<std::string, matvar_t*>* loadedVars;
} mat_file_internal_t;

#define GET_MATFP(fp) mat_file_internal_t* _file = static_cast<mat_file_internal_t*>((fp)); \
	mat_t* matfp = _file->matfp;

MORLIB_API mat_file_t MORLIB_NAMESPACE_PREFIX mat_file_open(const char* filePath)
{
	mat_file_internal_t* file = (mat_file_internal_t*)malloc(sizeof(mat_file_internal_t));
	file->matfp = Mat_Open(filePath, MAT_ACC_RDONLY);
	file->loadedVars = new std::map<std::string, matvar_t*>();
	return static_cast<mat_file_t>(file);
}

MORLIB_API void MORLIB_NAMESPACE_PREFIX mat_file_close(mat_file_t mfile)
{
	mat_file_internal_t* file = static_cast<mat_file_internal_t*>(mfile);
	if (file)
	{
		for (auto it = file->loadedVars->begin(); it != file->loadedVars->end(); it++)
		{
			Mat_VarFree(it->second);
		}
		file->loadedVars->clear();
		delete file->loadedVars;
		Mat_Close(file->matfp);
		free(file);
	}
}

MORLIB_API bool MORLIB_NAMESPACE_PREFIX mat_file_loaded(mat_file_t mfile)
{
	mat_file_internal_t* file = static_cast<mat_file_internal_t*>(mfile);
	if (file)
	{
		return file->matfp != NULL;
	}
	return false;
}

matvar_t* mat_find_variable(mat_file_internal_t* file, const char* name, bool cache = true)
{
	if (!file)
	{
		return NULL;
	}
	auto it = file->loadedVars->find(name);
	if (it != file->loadedVars->end())
	{
		return it->second;
	}
	auto var = Mat_VarReadInfo(file->matfp, name);
	if (!var)
	{
		Mat_Rewind(file->matfp);
		var = Mat_VarReadInfo(file->matfp, name);
	}
	if (var && cache)
	{
		(*file->loadedVars)[name] = var;
	}
	return var;
}

MORLIB_API bool MORLIB_NAMESPACE_PREFIX mat_file_is_float(mat_file_t file, const char* name)
{
	GET_MATFP(file);
	if (matfp)
	{
		auto var = mat_find_variable(_file, name);
		return var && var->class_type == MAT_C_SINGLE;
	}
	return false;
}

MORLIB_API bool MORLIB_NAMESPACE_PREFIX mat_file_is_double(mat_file_t file, const char* name)
{
	GET_MATFP(file);
	if (matfp)
	{
		auto var = mat_find_variable(_file, name);
		return var && var->class_type == MAT_C_DOUBLE;
	}
	return false;
}

MORLIB_API bool MORLIB_NAMESPACE_PREFIX mat_file_is_u16(mat_file_t file, const char* name)
{
	GET_MATFP(file);
	if (matfp)
	{
		auto var = mat_find_variable(_file, name);
		return var && var->class_type == MAT_C_UINT16;
	}
	return false;
}

MORLIB_API bool MORLIB_NAMESPACE_PREFIX mat_file_is_string_array(mat_file_t file, const char* name)
{
	GET_MATFP(file);
	if (matfp)
	{
		auto var = mat_find_variable(_file, name);
		if (var && var->class_type == MAT_C_CELL &&
			var->rank == 2 && var->dims[0] > 0 && var->dims[1] == 1)
		{
			auto cell = Mat_VarGetCell(var, 0);
			return cell->class_type == MAT_C_CHAR && cell->data_type == MAT_T_UINT8;
		}
	}
	return false;
}

MORLIB_API unsigned int MORLIB_NAMESPACE_PREFIX mat_file_get_dimensions(mat_file_t file, const char* name)
{
	GET_MATFP(file);
	if (matfp)
	{
		auto var = mat_find_variable(_file, name);
		if (var)
		{
			return static_cast<unsigned int>(var->rank);
		}
	}
	return 0;
}

uword mat_file_get_dim_value(mat_file_t file, const char* name, int dim)
{
	GET_MATFP(file);
	if (matfp)
	{
		auto var = mat_find_variable(_file, name);
		if (var)
		{
			uword res = 0;
			if (var->rank >= (dim + 1))
			{
				res = static_cast<uword>(var->dims[dim]);
			}
			return res;
		}
	}
	return 0;
}

MORLIB_API uword MORLIB_NAMESPACE_PREFIX mat_file_get_rows(mat_file_t file, const char* name)
{
	return mat_file_get_dim_value(file, name, 0);
}

MORLIB_API uword MORLIB_NAMESPACE_PREFIX mat_file_get_cols(mat_file_t file, const char* name)
{
	return mat_file_get_dim_value(file, name, 1);
}

MORLIB_API uword MORLIB_NAMESPACE_PREFIX mat_file_get_slices(mat_file_t file, const char* name)
{
	return mat_file_get_dim_value(file, name, 2);
}

MORLIB_API fmat MORLIB_NAMESPACE_PREFIX mat_file_load_matrix_float(mat_file_t file, const char* name, uword slice)
{
	GET_MATFP(file);
	fmat ret;
	if (matfp)
	{
		auto var = mat_find_variable(_file, name);
		if (var)
		{
			if (var->class_type == MAT_C_SINGLE && (var->rank <= 2 || (slice != ARMA_MAX_UWORD && var->rank <= 3)))
			{
				// Resize the matrix
				uword rows = static_cast<uword>(var->dims[0]);
				uword cols = 1;
				if (var->rank >= 2)
				{
					cols = static_cast<uword>(var->dims[1]);
				}
				ret.resize(rows, cols);

				// Load the data
				auto dataPtr = ret.memptr();
				if (var->rank == 3)
				{
					// Each element in the array is for a dimension.
					int start[3] = { 0, 0, static_cast<int>(slice) };
					int stride[3] = { 1, 1, 1 };
					int edge[3] = { static_cast<int>(rows), static_cast<int>(cols), 1 };
					if (Mat_VarReadData(matfp, var, dataPtr, start, stride, edge))
					{
						ret.reset();
					}
				}
				else
				{
					if (Mat_VarReadDataLinear(matfp, var, dataPtr, 0, 1, static_cast<int>(ret.n_elem)))
					{
						ret.reset();
					}
				}
			}
		}
	}
	return ret;
}

MORLIB_API fcube MORLIB_NAMESPACE_PREFIX mat_file_load_cube_float(mat_file_t file, const char* name)
{
	GET_MATFP(file);
	fcube ret;
	if (matfp)
	{
		auto var = mat_find_variable(_file, name);
		if (var)
		{
			if (var->class_type == MAT_C_SINGLE && var->rank == 3)
			{
				// Resize the cube
				uword rows = static_cast<uword>(var->dims[0]);
				uword cols = static_cast<uword>(var->dims[1]);
				uword slices = static_cast<uword>(var->dims[2]);
				ret.resize(rows, cols, slices);

				// Each element in the array is for a dimension.
				int start[3] = { 0, 0, 0 };
				int stride[3] = { 1, 1, 1 };
				int edge[3] = { static_cast<int>(rows), static_cast<int>(cols), static_cast<int>(slices) };

				// Load the data
				auto dataPtr = ret.memptr();
				if (Mat_VarReadData(matfp, var, dataPtr, start, stride, edge))
				{
					ret.reset();
				}
			}
		}
	}
	return ret;
}

MORLIB_API mat MORLIB_NAMESPACE_PREFIX mat_file_load_matrix_double(mat_file_t file, const char* name, uword slice)
{
	GET_MATFP(file);
	mat ret;
	if (matfp)
	{
		auto var = mat_find_variable(_file, name);
		if (var)
		{
			if (var->class_type == MAT_C_DOUBLE && (var->rank <= 2 || (slice != ARMA_MAX_UWORD && var->rank <= 3)))
			{
				// Resize the matrix
				uword rows = static_cast<uword>(var->dims[0]);
				uword cols = 1;
				if (var->rank >= 2)
				{
					cols = static_cast<uword>(var->dims[1]);
				}
				ret.resize(rows, cols);

				// Load the data
				auto dataPtr = ret.memptr();
				if (var->rank == 3)
				{
					// Each element in the array is for a dimension.
					int start[3] = { 0, 0, static_cast<int>(slice) };
					int stride[3] = { 1, 1, 1 };
					int edge[3] = { static_cast<int>(rows), static_cast<int>(cols), 1 };
					if (Mat_VarReadData(matfp, var, dataPtr, start, stride, edge))
					{
						ret.reset();
					}
				}
				else
				{
					if (Mat_VarReadDataLinear(matfp, var, dataPtr, 0, 1, static_cast<int>(ret.n_elem)))
					{
						ret.reset();
					}
				}
			}
		}
	}
	return ret;
}

MORLIB_API cube MORLIB_NAMESPACE_PREFIX mat_file_load_cube_double(mat_file_t file, const char* name)
{
	GET_MATFP(file);
	cube ret;
	if (matfp)
	{
		auto var = mat_find_variable(_file, name);
		if (var)
		{
			if (var->class_type == MAT_C_DOUBLE && var->rank == 3)
			{
				// Resize the cube
				uword rows = static_cast<uword>(var->dims[0]);
				uword cols = static_cast<uword>(var->dims[1]);
				uword slices = static_cast<uword>(var->dims[2]);
				ret.resize(rows, cols, slices);

				// Each element in the array is for a dimension.
				int start[3] = { 0, 0, 0 };
				int stride[3] = { 1, 1, 1 };
				int edge[3] = { static_cast<int>(rows), static_cast<int>(cols), static_cast<int>(slices) };

				// Load the data
				auto dataPtr = ret.memptr();
				if (Mat_VarReadData(matfp, var, dataPtr, start, stride, edge))
				{
					ret.reset();
				}
			}
		}
	}
	return ret;
}

MORLIB_API Mat<arma::u16> MORLIB_NAMESPACE_PREFIX mat_file_load_matrix_u16(mat_file_t file, const char* name, uword slice)
{
	GET_MATFP(file);
	Mat<arma::u16> ret;
	if (matfp)
	{
		auto var = mat_find_variable(_file, name);
		if (var)
		{
			if (var->class_type == MAT_C_UINT16 && (var->rank <= 2 || (slice != ARMA_MAX_UWORD && var->rank <= 3)))
			{
				// Resize the matrix
				uword rows = static_cast<uword>(var->dims[0]);
				uword cols = 1;
				if (var->rank >= 2)
				{
					cols = static_cast<uword>(var->dims[1]);
				}
				ret.resize(rows, cols);

				// Load the data
				auto dataPtr = ret.memptr();
				if (var->rank == 3)
				{
					// Each element in the array is for a dimension.
					int start[3] = { 0, 0, static_cast<int>(slice) };
					int stride[3] = { 1, 1, 1 };
					int edge[3] = { static_cast<int>(rows), static_cast<int>(cols), 1 };
					if (Mat_VarReadData(matfp, var, dataPtr, start, stride, edge))
					{
						ret.reset();
					}
				}
				else
				{
					if (Mat_VarReadDataLinear(matfp, var, dataPtr, 0, 1, static_cast<int>(ret.n_elem)))
					{
						ret.reset();
					}
				}
			}
		}
	}
	return ret;
}

MORLIB_API Cube<arma::u16> MORLIB_NAMESPACE_PREFIX mat_file_load_cube_u16(mat_file_t file, const char* name)
{
	GET_MATFP(file);
	Cube<arma::u16> ret;
	if (matfp)
	{
		auto var = mat_find_variable(_file, name);
		if (var)
		{
			if (var->class_type == MAT_C_UINT16 && var->rank == 3)
			{
				// Resize the cube
				uword rows = static_cast<uword>(var->dims[0]);
				uword cols = static_cast<uword>(var->dims[1]);
				uword slices = static_cast<uword>(var->dims[2]);
				ret.resize(rows, cols, slices);

				// Each element in the array is for a dimension.
				int start[3] = { 0, 0, 0 };
				int stride[3] = { 1, 1, 1 };
				int edge[3] = { static_cast<int>(rows), static_cast<int>(cols), static_cast<int>(slices) };

				// Load the data
				auto dataPtr = ret.memptr();
				if (Mat_VarReadData(matfp, var, dataPtr, start, stride, edge))
				{
					ret.reset();
				}
			}
		}
	}
	return ret;
}

#if 0
MORLIB_API Mat<arma::u16> MORLIB_NAMESPACE_PREFIX mat_file_load_matrix_cell(mat_file_t file, const char* name, uword slice)
{
	GET_MATFP(file);
	Mat<arma::u16> ret;
	if (matfp)
	{
		auto var = mat_find_variable(_file, name);
		if (var)
		{
			if (var->class_type == MAT_C_CELL && (var->rank <= 2 || (slice != ARMA_MAX_UWORD && var->rank <= 3)))
			{
				// Resize the matrix
				uword rows = static_cast<uword>(var->dims[0]);
				uword cols = 1;
				if (var->rank >= 2)
				{
					cols = static_cast<uword>(var->dims[1]);
				}
				ret.resize(rows, cols);

				auto j = Mat_VarGetCell(var, 1);
				if (j->class_type == MAT_C_CHAR && j->data_type == MAT_T_UINT8 && j->dims[0] == 1)
				{
					char* str = (char*)malloc(j->dims[1] + 1);
					if (!Mat_VarReadDataAll(matfp, j))
					{
						memcpy(str, j->data, j->dims[1]);
					}
					str[j->dims[1]] = '\0';
					free(str);
				}
			}
		}
	}
	return ret;
}
#endif

MORLIB_API const char* const* MORLIB_NAMESPACE_PREFIX mat_file_load_string_array(mat_file_t file, const char* name)
{
	char** ret = NULL;
	if (mat_file_is_string_array(file, name))
	{
		GET_MATFP(file);
		auto var = mat_find_variable(_file, name);

		//XXX potential other loading option: count all cell dims, create one string array, copy data into a section of the array with the returned array really just being different pointers

		// Get the cells and allocate the array to return
		matvar_t** cells = Mat_VarGetCellsLinear(var, 0, 1, static_cast<int>(var->dims[0]));
		ret = (char**)calloc(var->dims[0] + 1, sizeof(char*));
		if (cells && ret)
		{
			uword count = static_cast<uword>(var->dims[0]);
			for (uword i = 0; i < count; i++)
			{
				ret[i] = (char*)calloc(cells[i]->dims[1] + 1, sizeof(char));
				if (ret[i])
				{
					if (cells[i]->data || !Mat_VarReadDataAll(matfp, cells[i]))
					{
						memcpy(ret[i], cells[i]->data, cells[i]->dims[1]);
					}
					else
					{
						free((void*)cells);
						cells = NULL;
						break;
					}
				}
				else
				{
					free((void*)cells);
					cells = NULL;
					break;
				}
			}
		}
		if (cells)
		{
			free((void*)cells);
		}
		else
		{
			mat_file_free_string_array((const char**)ret);
			ret = NULL;
		}
	}
	return (const char* const*)ret;
}

MORLIB_API void MORLIB_NAMESPACE_PREFIX mat_file_free_string_array(const char* const* arr)
{
	if (arr)
	{
		for (arma::uword i = 0; arr[i]; i++)
		{
			free((void*)arr[i]);
		}
		free((void*)arr);
	}
}
