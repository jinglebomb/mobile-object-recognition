/*
 * Copyright(c) 2014 - 2015 Vincent Simonetti
 * Stevens Institute of Technology
 * vsimonet at stevens edu
 *
 * Copyright(c) 2014 - 2015 Krzysztof Jordan
 * Stevens Institute of Technology
 * kjordan1 at stevens edu
 *
 * --------------------------------------------------------------
 *
 * "random_sample", "is_colinear"
 *
 * Copyright(c) 2006 Peter Kovesi
 * School of Computer Science & Software Engineering
 * The University of Western Australia
 * pk at csse uwa edu au
 * http://www.csse.uwa.edu.au/~pk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files(the "Software"), to deal
 * in the Software without restriction, subject to the following conditions :
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * The Software is provided "as is", without warranty of any kind.
 *
 * September 2006
 * September 2014 - Ported to C/C++ with Armadillo
 */

#include <functional>

#include <c/util.h>

using namespace arma;
MORLIB_NAMESPACE_USE;

MORLIB_API umat MORLIB_NAMESPACE_PREFIX random_sample(uword a, uword n)
{
	auto m = fill_index(umat(1, a));
	return random_sample(m, n);
}

MORLIB_API umat MORLIB_NAMESPACE_PREFIX random_sample(umat& a, uword n)
{
	auto npts = length(a);

	if (npts == 1)
	{
		// We have a scalar argument for 'a'
		return random_sample(a[0], n);
	}

	if (npts < n)
	{
		printf("random_sample: Trying to select %d items from a list of length %d\n", n, npts);
		return umat();
	}

	umat item = zeros<umat>(1, n);

	for (uword i = 0; i < n; i++)
	{
		// Generate random value in the appropriate range
		auto r = static_cast<uword>(ceilf(static_cast<float>(npts - i - 1) * matlab_randf()));
		item[i] = a[r];         // Select the rth element from the list
		a[r] = a[npts - i - 1]; // Overwrite selected element
	}

	return item;
}

MORLIB_API unsigned int MORLIB_NAMESPACE_PREFIX generate_seed(const fmat& x)
{
	std::hash<const float*> hash_gen;
	auto seed = hash_gen(x.memptr());
	return static_cast<unsigned int>(seed * seed);
}

MORLIB_API bool MORLIB_NAMESPACE_PREFIX is_colinear(const fvec& p1, const fvec& p2, const fvec& p3, bool homogneeous)
{
	if (p1.n_elem != p2.n_elem || p1.n_elem != p3.n_elem || !(p1.n_elem == 2 || p1.n_elem == 3))
	{
		// points must have the same dimension of 2 or 3
		return false;
	}

	/*
	 * If data is 2D, assume they are 2D inhomogeneous coords. Make them
	 * homogeneous with scale 1.
	 */
	fvec point1 = p1;
	fvec point2 = p2;
	fvec point3 = p3;
	if (p1.n_elem == 2)
	{
		point1.resize(3);
		point2.resize(3);
		point3.resize(3);
		point1[2] = 1;
		point2[2] = 1;
		point3[2] = 1;
	}

	if (homogneeous)
	{
		/*
		 * Apply test that allows for homogeneous coords with arbitrary
		 * scale.  p1 X p2 generates a normal vector to plane defined by
		 * origin, p1 and p2.  If the dot product of this normal with p3
		 * is zero then p3 also lies in the plane, hence co-linear.
		 */
		//XXX somewhere... deep within the bowels of the "dot" template... a template metaprogramming clause for checking data types says that float == double, and returns a double. Only for everything else to say it's float, and cause a compiler warning...
		return abs(dot(cross(point1, point2), point3)) < eps(1.0f);
	}
	else
	{
		// Assume inhomogeneous coords, or homogeneous coords with equal scale.
		return norm(cross(point2 - point1, point3 - point1)) < eps(1.0f);
	}
}

fmat depth_sub_image_apply(const fmat& i, uword x, uword y, uword w, uword h, depth_convert_settings settings, uvec* actual_indicies, bool abs_indicies,
	std::function<void(float* ptr, float d, uword x, uword y, uword w, uword h)> op)
{
	// Bounds checks
	i(y, x);
	i(y + h - 1, x + w - 1);

	fmat result(w * h, 3);
	uvec act_ind_ref(actual_indicies ? w * h : 1);
	auto act_ind = act_ind_ref.memptr();

	frowvec3 row;
	auto row_ptr = row.memptr();
	w += x;
	h += y;

	//XXX re-evaluate how abs_indicies should work
	uword row_ind = 0;
	uword element_index = abs_indicies ? (y + x * i.n_rows) : 0;
	uword element_offset = abs_indicies ? (2 * y + h - 1) : 0;
	for (auto xi = x; xi < w; xi++)
	{
		for (auto yi = y; yi < h; yi++)
		{
			element_index++;
			auto r = i.at(yi, xi);
			if (settings != DEPTH_CONVERT_ALL && r <= 0 &&
				((settings != DEPTH_CONVERT_ALL_NEGATIVE_SET_DEPTH) && (settings != DEPTH_CONVERT_ALL_ZERO_NEGATIVE_SET_DEPTH)))
			{
				if ((settings == DEPTH_CONVERT_IGNORE_NEGATIVE && r < 0) ||
					(settings == DEPTH_CONVERT_IGNORE_ZERO_NEGATIVE)) // This check was done in the outer 'if'
				{
					continue;
				}
			}
			op(row_ptr, r, xi, yi, i.n_cols, i.n_rows);
			if ((settings == DEPTH_CONVERT_ALL_NEGATIVE_SET_DEPTH && r < 0) ||
				(settings == DEPTH_CONVERT_ALL_ZERO_NEGATIVE_SET_DEPTH))
			{
				row_ptr[2] = r;
			}
			result.row(row_ind++) = row;
			if (actual_indicies)
			{
				act_ind[row_ind - 1] = element_index - 1;
			}
		}
		if (abs_indicies)
		{
			element_index += element_offset;
		}
	}
	if (settings != DEPTH_CONVERT_ALL && row_ind != result.n_rows)
	{
		result.resize(row_ind, result.n_cols);
	}
	if (actual_indicies)
	{
		if (row_ind != result.n_rows)
		{
			act_ind_ref.resize(result.n_rows);
		}
		*actual_indicies = act_ind_ref;
	}
	return result;
}

MORLIB_API fmat MORLIB_NAMESPACE_PREFIX depth_sub_image_to_point_cloud(const fmat& i, uword x, uword y, uword w, uword h, float fh, float fv, depth_convert_settings settings, uvec* actual_indicies, bool abs_indicies)
{
	auto th = tanf(fh / 2.0f);
	auto tv = tanf(fv / 2.0f);
	return depth_sub_image_apply(i, x, y, w, h, settings, actual_indicies, abs_indicies, [th, tv](float* ptr, float depth, uword j, uword i, uword width, uword height)
	{
		ptr[0] = depth * (((float)j / (width - 1)) - 0.5f) * th;
		ptr[1] = depth * (((float)i / (height - 1)) - 0.5f) * tv;
		ptr[2] = depth;
	});
}

MORLIB_API fmat MORLIB_NAMESPACE_PREFIX depth_sub_image_to_point_cloud(const fmat& i, uword x, uword y, uword w, uword h, float fl, depth_convert_settings settings, uvec* actual_indicies, bool abs_indicies)
{
	auto ifl = 1.0f / fl;
	return depth_sub_image_apply(i, x, y, w, h, settings, actual_indicies, abs_indicies, [ifl](float* ptr, float depth, uword j, uword i, uword, uword)
	{
		ptr[0] = depth * ifl * j;
		ptr[1] = depth * ifl * i;
		ptr[2] = depth;
	});
}

MORLIB_API fmat MORLIB_NAMESPACE_PREFIX point_cloud_sub_window(const fmat& XYZ, uword XYZ_width, uword XYZ_height, uword x, uword y, uword w, uword h, depth_convert_settings settings, uvec* actual_indicies)
{
	arma_debug_check(((x + w) > XYZ_width), "x + w must be less then or equal to XYZ_width.");
	arma_debug_check(((y + h) > XYZ_height), "y + h must be less then or equal to XYZ_height.");

	uvec act_ind_ref(w * h);
	auto act_ind = act_ind_ref.memptr();
	uword index_count = 0;

	w += x;
	h += y;

	// Build index list
	for (auto xi = x; xi < w; xi++)
	{
		for (auto yi = y; yi < h; yi++)
		{
			auto index = yi + xi * XYZ_height;
			auto depth = XYZ.at(index, 2);
			if (settings != DEPTH_CONVERT_ALL && depth <= 0 &&
				((settings != DEPTH_CONVERT_ALL_NEGATIVE_SET_DEPTH) && (settings != DEPTH_CONVERT_ALL_ZERO_NEGATIVE_SET_DEPTH)))
			{
				if ((settings == DEPTH_CONVERT_IGNORE_NEGATIVE && depth < 0) ||
					(settings == DEPTH_CONVERT_IGNORE_ZERO_NEGATIVE)) // This check was done in the outer 'if'
				{
					continue;
				}
			}
			act_ind[index_count++] = index;
		}
	}

	if (actual_indicies)
	{
		if (act_ind_ref.n_elem != index_count)
		{
			act_ind_ref.resize(index_count);
		}
		*actual_indicies = act_ind_ref;
	}
	return XYZ.rows(act_ind_ref);
}

MORLIB_API void MORLIB_NAMESPACE_PREFIX write_ply(const char* file_str, const fmat& XYZ, const umat& colors)
{
	bool print_colors = colors.n_rows == XYZ.n_rows && XYZ.n_rows > 0;

	std::ofstream file = std::ofstream(file_str);
	file << "ply" << std::endl << "format ascii 1.0" << std::endl
		<< "element vertex " << XYZ.n_rows << std::endl
		<< "property float32 x" << std::endl
		<< "property float32 y" << std::endl
		<< "property float32 z" << std::endl;
	if (print_colors)
	{
		file << "property uchar red" << std::endl
			<< "property uchar green" << std::endl
			<< "property uchar blue" << std::endl;
	}
	file << "end_header" << std::endl;

	for (auto i = uword(0); i < XYZ.n_rows; i++)
	{
		const auto r = XYZ.row(i);
		file << r(0) << " " << r(1) << " " << r(2);
		if (print_colors)
		{
			const auto c = colors.row(i);
			file << " " << c(0) << " " << c(1) << " " << c(2);
		}
		file << std::endl;
	}

	file.flush();
	file.close();
}

#ifdef _WIN32
#include <Windows.h>
double __counterCountToMS = 0.0;
#endif

MORLIB_API double MORLIB_NAMESPACE_PREFIX counterMS()
{
#ifdef _WIN32
	if (__counterCountToMS == 0LL)
	{
		LARGE_INTEGER tps;
		QueryPerformanceFrequency(&tps);
		__counterCountToMS = (double)(tps.QuadPart / 1000L);
	}
	LARGE_INTEGER queryTime;
	QueryPerformanceCounter(&queryTime);
	return queryTime.QuadPart / __counterCountToMS;
#else
	//TODO
	return 0.0;
#endif
}
