Mobile Object Recognition
=========================

Intro
-----

Recognize objects, in real time, on a mobile device. Allowing for active processing of an individual’s surroundings, augmented reality, and possibly artificial intelligence application within modern smart phones, and tablets.

Setup
-----

1. Create a directory for the Mobile Object Recognition project and libraries (we'll call it "mor")
2. Clone the repository into "mor" directory.
3. Run install.bat/install.sh
4. Compile Mobile Object Recognition solution

About
-----

Stevens Institute of Technology

Team Members:

* Krzysztof Jordan - kjordan1 at stevens edu
* Zachary Smith    - zsmith at stevens edu
* Neil Hinrichs    - nhinrich at stevens edu
* Vinnie Simonetti - vsimonet at stevens edu

Advisor: Philippos Mordohai - philippos dot mordohai at stevens edu
